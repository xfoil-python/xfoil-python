# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).


## [Unreleased]

### Added

- Keyword `as_dict=True` added to `a()`, `cl()`, `aseq()` and `cseq()` methods
  to return the calculation result as a dictionary (instead of a tuple).
- Methods `arange()` and `clrange()` added, which are extended versions of the
  original `aseq()` and `cseq()` methods. (Closes #5)
- `Airfoil` can be imported with `from xfoil import Airfoil` or used as
  `import xfoil; af = xfoil.Airfoil(...)`.
- `Airfoil` data can be saved to and loaded from a file. (Closes #12)

### Changed

### Deprecated

- Setting the `coord`, `x` and `y` properties in `model.Airfoil` is now
  deprecated.

### Removed

### Fixed

- The model x- and y-coordinates now accept any object, which can be converted
  to a numpy array, such as lists, iterables and also numpy arrays.

### Security


## [1.1.1] - 2019-10-25

Last version published by Daniel de Vries.
