# -*- coding: utf-8 -*-
"""Module for `xfoil-python` with geometric models.

Copyright (C) 2019 D. de Vries
Copyright (C) 2019 DARcorporation

This file is part of xfoil-python.

xfoil-python is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

xfoil-python is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with xfoil-python.  If not, see <https://www.gnu.org/licenses/>.

"""

import numpy as np


class Airfoil:
    """Geometry of an airfoil.

    Attributes
    ----------
    coords
    n_coords
    x
    y
    """

    def __init__(self, x, y, fname=None, **kwargs):
        """Initizialize the `Airfoil` class.

        If fname is not None, load the data from a file. (In that case the two
        arguments x and y are not used, but still required, so it is best to
        set them to None.)

        Parameters
        ----------
        x : iterable or np.ndarray-like object
            List of x-coordinates of the airfoil surface.
        y : iterable or np.ndarray-like object
            List of y-coordinates of the airfoil surface.
        fname : file handle, str, pathlib.Path, list of str, generator
            File, filename, list, or generator to read.  If the filename
            extension is ``.gz`` or ``.bz2``, the file is first decompressed.
            Note that generators must return bytes or strings. The strings
            in a list or produced by a generator are treated as lines.
        **kwargs
            The keyword arguments are passed to the np.loadtext function (see
            there).
        """
        if fname:
            self._load(fname, **kwargs)
        else:
            x_array = np.ravel(x)
            y_array = np.ravel(y)
            if x_array.size != y_array.size:
                raise ValueError(
                    f"The lists of x- and y-coordinates "
                    f"do not have the same number of points. "
                    f"Got: len(x)={x_array.size}, len(y)={y_array.size}")
            self._coords = np.ndarray((x_array.size, 2))
            self._coords[:, 0] = x_array
            self._coords[:, 1] = y_array

    @property
    def coords(self):
        """np.ndarray: List of pairs with x- and y-coordinates."""
        return self._coords

    @coords.setter
    def coords(self, value):
        raise DeprecationWarning(
            f"Please do not set the coordinates after having initialized "
            f"the '{self.__class__.__name__}' class. "
            f"The property 'coords' will become readable in future versions.")
        self._coords = value

    @property
    def n_coords(self):
        """int: Number of coordinates which define the airfoil surface."""
        return self._coords.shape[0]

    @property
    def x(self):
        """np.ndarray: List of x-coordinates of the airfoil surface."""
        return self._coords[:, 0]

    @x.setter
    def x(self, value):
        raise DeprecationWarning(
            f"Please do not set the x-coordinates after having initialized "
            f"the '{self.__class__.__name__}' class. "
            f"The property 'x' will become readable in future versions.")
        v = np.ravel(value)
        self._coords = np.resize(self._coords, (v.size, 2))
        self._coords[:, 0] = v[:]

    @property
    def y(self):
        """np.ndarray: List of y-coordinates of the airfoil surface."""
        return self._coords[:, 1]

    @y.setter
    def y(self, value):
        raise DeprecationWarning(
            f"Please do not set the y-coordinates after having initialized "
            f"the '{self.__class__.__name__}' class. "
            f"The property 'y' will become readable in future versions.")
        v = np.ravel(value)
        self._coords = np.resize(self._coords, (v.size, 2))
        self._coords[:, 1] = v[:]

    def _load(self, fname, **kwargs):
        """Load and store the coordinates from text file 'fname'.

        Parameters
        ----------
        fname : file handle, str, pathlib.Path, list of str, generator
            File, filename, list, or generator to read.  If the filename
            extension is ``.gz`` or ``.bz2``, the file is first decompressed.
            Note that generators must return bytes or strings. The strings
            in a list or produced by a generator are treated as lines.
        **kwargs
            The keyword arguments are passed to the np.loadtext function (see
            there).
        """
        values = np.loadtxt(fname, **kwargs)
        if values.shape[1] != 2:
            raise ValueError(
                f"The file must contain exactly two columns with the "
                f"x- and y-coordinates of the airfoil surface. "
                f"Got: {values.size[1]} columns")
        self._coords = values

    def save(self, fname, **kwargs):
        """Save the coordinates to text file 'fname'.

        Parameters
        ----------
        fname : str or file handle
            If the filename ends in `.gz`, the file is automatically saved in
            compressed gzip format.
        **kwargs
            The keyword arguments are passed to the np.savetext function (see
            there).
        """
        np.savetxt(fname, self._coords, **kwargs)
