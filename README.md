# General

This is a stripped down version of XFOIL, presented in the form of a Python
module. What's unique about this package w.r.t. many others out there allowing
an interface to XFOIL, is the fact that the Python code talks directly to a
compiled Fortran library. This approach avoids having to read/write in-/output
files to the disk and communicating with the XFOIl executable. Eliminating the
need for constant disk I/O operations can significantly speed up parallel
frameworks in particular, giving this approach a clear advantage.


# Building and Installing the Python Module

## Windows

If you are on a Windows machine (64bit) with Python 3.6, installing XFoil is a
simple matter of running:

```bash
pip install xfoil
```

## Linux

(New in v1.2.0)
On Linux the XFoil Fortran code has to be compiled. This is done automatically,
if all required packages are installed
```bash
pip install xfoil
```

## Manual Installation

If you are using a different type of machine, or a different version of Python,
you will have to make sure some software is installed on your system in order
for the package to be successfully built. First of all, the module targets
Python 3, and does NOT support Python 2. So make sure a Python 3 environment is
installed and available. Furthermore, working compilers for C and Fortran have
to be installed and on the PATH. On Windows, the build and installation have
ONLY been tested with MinGW, using gcc and gfortran.

Then, installing XFoil should be as simple as running

```bash
pip install .
```

from the root of the downloaded repository.

On Windows, you may have to force the system to use MinGW. To do so, create a
file named `distutils.cfg` in
`PYTHONPATH\Lib\distutils` with the following contents:

```INI
[build]
compiler=mingw32
```

If you are not able to create this file for your Python environment, you can
instead create a file named `setup.cfg` in the root of the repo with the same
contents. It is also possible to force the use of MinGW  directly when invoking
`pip` by calling:

```bash
pip install --global-option build_ext --global-option --compiler=mingw32 .
```


# Quickstart

All XFoil operations are performed using the `XFoil` class. So the first step
when using this module is to create an instance of this class:

```pycon
>>> from xfoil import XFoil
>>> xf = XFoil()
```

If this does not produce any errors, the installation should be functioning
properly.


The symmetric NACA 0012 airfoil is included as a test case. It can be loaded
into the XFoil library like this:

```pycon
>>> from xfoil.test import naca0012
>>> xf.airfoil = naca0012

 Number of input coordinate points: 160
 Counterclockwise ordering
 Max thickness =     0.120008  at x =   0.308
 Max camber    =     0.000000  at x =   0.033

  LE  x,y  =  -0.00000   0.00000  |   Chord =   1.00000
  TE  x,y  =   1.00000   0.00000  |

 Current airfoil nodes set from buffer airfoil nodes ( 160 )
```

Once the airfoil has been loaded successfully it can be analyzed. Let us
analyze it for a range of angles of attack, at a Reynolds number of one
million. Let's limit the maximum number of iterations to 40 (the default is 20)
as well. For the range of angles of attack, we will go from -20° to 20° with
steps of 0.5°:

```pycon
>>> xf.Re = 1e6
>>> xf.max_iter = 40
>>> res = xf.arange(-20, 20, 0.5, as_dict=True)
```

The XFOIL library should produce a lot of output, which should be familiar to
those who have used the original XFOIL application before. This output can be
switched off with

```pycon
>>> xf.print = False
```

The final result is a dictionary with lists of angles of attack, `a`, and the
corresponding lift coefficients, `cl`, drag coefficients, `cd`, moment
coefficients, `cm`, and minimum pressure coefficients `cp`. We can now, for
example, plot the lift curve for this airfoil:

```pycon
>>> import matplotlib.pyplot as plt
>>> plt.plot(res['a'], res['cl'])
>>> plt.show()
```

This should produce the following figure:

![NACA 0012 Lift Curve](https://github.com/daniel-de-vries/xfoil-python/raw/master/naca0012-lift-curve.png)

Just like in the original XFOIL application, an airfoil can also analyzed for a
single angle of attack, single lift coefficient, or a range of lift
coefficients. The commands for these operations are (note, that as we set
`as_dict=False`, which is the default, we receive a tuple with the lists):

To analyze for an angel of attack of 10°:
```pycon
>>> cl, cd, cm = xf.a(10)
```

To analyze for a lift coefficient of 1.0:
```pycon
>>> a, cd, cm = xf.cl(1)
```

And for a range of lift coefficients from -0.5 to 0.5 with steps of 0.05.
```pycon
>>> a, cl, cd, cm, cp = xf.clrange(-0.5, 0.5, 0.05)
```

For other features and specifics, see the documentation API documentation
below (and as always the documentation in the Python source files).


# API

## Class `xfoil.Airfoil(x, y)`

The `Airfoil` class holds the airfoil's geometry.

Initialize the class with two lists/iterables/arrays; the first contains the
x-coordinates and the second the y-coordinates of the airfoil surface. Keep
the x-coordinates in an order which XFOIL can understand.

### Properties

#### Property `x` (readonly)

List of the x-coordinates of the airfoil surface.

#### Property `y` (readonly)

List of the y-coordinates of the airfoil surface corresponding to `x`.

#### Property `n_coords` (readonly)

Number of coordinates, which define the airfoil surface.

#### Property `coords` (readonly)

A `numpy` array  of the shape `(n_coords, 2)` containing the pairs with the
x- and y-coordinates of the airfoil surface .

### Example

```pycon
>>> import xfoil
>>> x = [...]
>>> y = [...]
>>> af = xfoil.Airfoil(x, y)
```


## Class `xfoil.XFoil()`

The `XFoil` class gives access to the XFOIL library.

### Properties

#### Property `print` (read/writeable)

If `True`, the console output should be shown.

#### Property `airfoil` (read/writeable)

An instance of the `Airfoil` class.

#### Property `Re` (read/writeable)

The Reynolds number.

#### Property `M` (read/writeable)

The Mach number.

#### Property `xtr` (read/writeable)

The top and bottom flow trip x/c locations (a `tuple(float, float)`)

#### Property `n_crit` (read/writeable)

The critical amplification ratio (a `float`).

#### Property `max_iter` (read/writeable)

Maximum number of iterations (an `int`).

### Methods

#### Method `naca(specifier)`

Set a NACA 4 or 5 series airfoil.

The `specifier` is an integer number with a NACA 4 or 5 series identifier,
such as `2412`.

#### Method `reset_bls()`

Reset boundary layers to be reinitialized on the next analysis.

#### Method `repanel(n_nodes=160, cv_par=1, cte_ratio=0.15, ctr_ratio=0.2,
                xt_ref=(1, 1), xb_ref=(1, 1))`

Re-panel the airfoil.

It takes the following parameters:
- `n_nodes` (`int`): The number of panel nodes.
- `cv_par` (`float`): The panel bunching parameter.
- `cte_ratio` (`float`): The trailing/leading edge density ratio.
- `ctr_ratio` (`float`): Refined-area/leading edge panel density ratio
- `xt_ref` (`tuple(float, float)`): Top side refined area x/c limits.
- `xb_ref` (`tuple(float, float)`): Bottom side refined area x/c limits.

#### Method `filter(factor=0.2)`

Filter surface speed distribution using modified Hanning filter.

`factor` is the filter parameter and a `float` value. If set to 1,
the standard, full Hanning filter is applied.

#### Method `a(a, as_dict=False)`

Analyze the airfoil at a fixed angle of attack `a`.

`a` is the angle of attack in degrees.

It returns the following four values (if `as_dict == True`, it returns a
dictionary):
- `cl`: The lift coefficient at angle of attack `a`.
- `cd`: The drag coefficient at angle of attack `a`.
- `cm`: The moment coefficient at angle of attack `a`.
- `cp`: The minimum pressure coefficient at angle of attack `a`.

#### Method `cl(cl, as_dict=False)`

Analyze the airfoil at a fixed lift coefficient `cl`.

`cl` is the lift coefficient

It returns the following four values (if `as_dict == True`, it returns a
dictionary):
- `a`: The angle of attack corresponding to `cl`.
- `cd`: The drag coefficient corresponding to `cl`.
- `cm`: The moment coefficient corresponding to `cl`.
- `cp`: The minimum pressure coefficient corresponding to `cl`.

#### Method `arange(a_start, a_end, a_step, endpoint=False, as_dict=False)`

Analyze the airfoil at a sequence of angles of attack.

The analysis is done for the angles of attack given by
`range(a_start, a_end, a_step)`, the start, end and increment angles. If
`endpoint` is `True`, `a_end` will be included in the range, otherwise it
behaves like the original Python `range()` function.

This is a wrapper around the original XFOIL `aseq` command.

It returns the following five lists (if `as_dict == True`, it returns a
dictionary):
- `a`: The list of angles of attack.
- `cl`: The list of lift coefficients at all angles of attack `a`.
- `cd`: The list of drag coefficients at all angles of attack `a`.
- `cm`: The list of moment coefficients at all angles of attack `a`.
- `cp`: The list of minimum pressure coefficients at all angles of attack `a`.

#### Method `aseq(a_start, a_end, a_step, as_dict=False)`

`aseq()` calls `arange()` with `endpoint=False`, which is the behaviour of the
original XFOIL command.

#### Method `clrange(a_start, a_end, a_step, endpoint=False, as_dict=False)`

Analyze the airfoil at a sequence of lift coefficients.

The analysis is done for the lift coefficients given by
`range(cl_start, cl_end, cl_step)`, the start, end, and increment lift
coefficients for the range. If `endpoint` is `True`, `a_end` will be included
in the range, otherwise it behaves like the original Python `range()` function.

This is a wrapper around the original XFOIL `cseq` command.

It returns the following five lists (if `as_dict == True`, it returns a
dictionary):
- `a`: The list of angles of attack.
- `cl`: The list of lift coefficients at all angles of attack `a`.
- `cd`: The list of drag coefficients at all angles of attack `a`.
- `cm`: The list of moment coefficients at all angles of attack `a`.
- `cp`: The list of minimum pressure coefficients at all angles of attack `a`.

#### Method `cseq(cl_start, cl_end, cl_step, as_dict=False)`

`cseq()` calls `clrange()` with `endpoint=False`, which is the behaviour of the
original XFOIL command.

#### Method `get_cp_distribution()`

Get the Cp distribution from the last converged point.

It returns the following three `numpy.array`s:
- `x`: The x-coordinates.
- `y`: The y-coordinates.
- `cp`: The pressure coefficients at the corresponding x-coordinates.

### Example

```pycon
>>> import xfoil
>>> af = xfoil.Airfoil(...)
>>> xf = xfoil.XFoil()
>>> xf.airfoil = af
>>> xf.arange(-2, 3, 1, endpoint=True, as_dict=True)
{'a': [-2, -1, 0, 1, 2, 3], 'cl': [...], 'cd': [...], 'cm': [...], 'cp': [...]}
```
